function build() {
    for directory in $(ls -d charts/*/); do
        echo "Packaging $directory"
        helm package $directory -d public
    done

    helm repo index public --url $CI_ENVIRONMENT_URL
    cp index.html public
}
